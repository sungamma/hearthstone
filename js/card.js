
class Action {
    constructor(config) {
        for (let k in config) {
            this[k] = config[k];
        }
    }

    get_error (ourHero, opponentHero, target, agent) {
        // пока что это правило для одного конретного случая -- атаки
        if(agent.type === "spell") return null;
        if(ourHero.minions.includes(target)) return "You cannot attack ally minions!";

        return null;
    }

    perform (h1, h2, target, agent) {
        if(agent.type === 'minion') {
            target.hp -= agent.attack;
            agent.hp -= target.attack;

        //   если есть доп экшены у агента -- то будет и тут тоже свич\кейс
        }
        else {
            switch (this.applyment) {
                case -1:
                    target.hand.push(...target.deck.splice(0, this.draw));
                    break;

                case 1:
                    if (this.polymorph) {
                        target.name = this.polymorph;
                        target.attack = this.attack;
                        target.hp = this.hp;
                    }
                    target.hp += this.damage;
                    target.freeze = this.freeze;
                    h1.hand.push(...h1.deck.splice(0, this.draw));
                    break;

                case 7:
                    h2.minions.map(enemy => {
                        enemy.hp += this.dealDamade;
                        enemy.freeze = this.freeze;
                    })

                    break;
            }
            h1.current_manna -= agent.manna;
        }
    }

}


class Card {
    constructor(name) {
        name = name.split('/');
        this.id = String(Math.random()).slice(2);
        this.type = name[0];
        this.name = name[1];


        const config = DB[name[1]] || {manna: 1, actionProps: {applyment: 0}}

        this.hp = config.hp || null;
        this.attack = config.attack || null;
        this.active = config.active || null;
        this.manna = config.manna;
        this.action = new Action(config.actionProps);
    }
}


// APPLYMENTS:

// -8: all our characters
// -1-7: 1 minion on our table
// -1: our hero only
// 0: enemy hero only
// 1 - 7: 1 minion on table
// 8: all enemies(hero + table)
// 100: all characters