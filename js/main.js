(async function() {
    const resp = await fetch('config.json');

    window.DB = await resp.json();

    const playground = new Playground(),
        deck1 = ['spell/fireball', 'minion/dire-mole', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/fireball', 'minion/dire-mole', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/fireball', 'minion/dire-mole', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', ],
        deck2 = ['spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/fireball', 'minion/dire-mole', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/fireball', 'minion/dire-mole', 'spell/flamestrike', 'minion/river-crocolisk', 'spell/flamestrike', 'minion/river-crocolisk', ],
        hero1 = new Hero('paladin', deck1),
        hero2 = new Hero('hunter', deck2);


    playground.start(hero1, hero2);

})();


