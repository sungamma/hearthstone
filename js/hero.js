

class Hero {
    constructor(type, deck) {
        this.type = type;
        this.manna = 1;
        this.current_manna = 1;
        this.deck = deck.map(name => new Card(name));
        this.hand = [];
        this.minions = [];
        this.hp = 30;
        this.attack = 0;
    }

    getCardById(place, id) {
        return this[place].filter(c => c.id === id)[0];
    }

    removeCard(place, card) {
        this[place] = this[place].filter(c => c.id !== card.id);
    }
}
