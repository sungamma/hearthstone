
class Playground {
    constructor() {
        const handlers = {
            '.hand-line .card-line': 'onHandCardSelect',
            '.player-table > .card-line': 'onMinionSelect',
            '.hero': 'onHeroSelect',
            '.turn_button': 'onReturn'
        }

        this.view = Object.fromEntries([...document.querySelectorAll('[id]')].map(el=>[el.id, el]));
        for (let selector in handlers) {
            document.querySelectorAll(selector).forEach(el => el.onclick = (ev) => this[handlers[selector]](ev.target));
        }

        this.current_turn = Math.floor(2 * Math.random())  + 1;
    }

    onReturn() {
        let hero = this.heroes[this.current_turn],
            card;

        if(hero.manna < 11) hero.manna++;
        this.current_turn = 3 - this.current_turn;

        hero = this.heroes[this.current_turn];
        hero.current_manna = hero.manna;
        let tempArray = [];
        for(let i = 0; i<hero.minions.length;i++){
            let el = hero.minions[i];
            el.active = true;
            tempArray.push(el);
        }

        card = hero.deck.splice(Math.floor(Math.random() * hero.deck.length), 1)[0];
        card && hero.hand.length <= 10 && hero.hand.push(card);
        this.active_card = null;
        this.updateView();
    }

    gameStatusCheck() {
        if(this.hero1.hp < 1) {
            alert(`${this.hero2.type} is a winner!`);
            this.view.playertable1.innerHTML = '';
        }
        if(this.hero2.hp < 1) {
            alert(`${this.hero1.type} is a winner!`);
            this.view.playertable2.innerHTML = '';
        }

    }

    onHandCardSelect(target) {
        console.log(target.getAttribute('card-id'))
        const card_id = target.getAttribute('card-id'),
            hero = this.getCurrentHero(),
            card = hero.getCardById('hand', card_id);
            console.log(target.parentNode)
        let error;

        if (target.parentNode.parentNode.id === `hand${this.current_turn}`) {
            if (hero.current_manna < card.manna) {
                return alert('Sorry, not enough manna');
            }


            switch (card.type) {
                case 'minion':
                    hero.current_manna -= card.manna;
                    hero.removeCard('hand', card);
                    hero.minions.push(card);
                    break;

                case 'spell':
                    switch (card.action.applyment) {
                        case 0:
                        case 7:
                            card.action.perform(...this.getHeroes(), null, card);
                            hero.removeCard('hand', card);
                            break;

                        case 1:
                            if (!this.active_card) {
                                this.active_card = card;
                            } else {
                                error = this.active_card.action.get_error(...this.getHeroes(), card, this.active_card);
                                if (error) {
                                    alert(error);
                                } else {
                                    this.active_card.action.perform(...this.getHeroes(), card, this.active_card);
                                    hero.removeCard('hand', card);
                                    this.updateView();
                                }
                            }

                            break;
                    }
                    break;
            }
            this.updateView();
        }

    }

    onMinionSelect(target) {
        let card_id = target.getAttribute('card-id'),
            card = this.hero1.getCardById('minions', card_id) || this.hero2.getCardById('minions', card_id),
            error;

        if (!this.active_card) {
            this.active_card = card;
        } else {
            error = this.active_card.action.get_error(...this.getHeroes(), card, this.active_card);
            if (error) {
                alert(error);
                this.active_card = null;
            } else {
               if (this.active_card.active || this.active_card.type === "spell") { //my
                   console.log(this.active_card.active);
                   this.active_card.action.perform(...this.getHeroes(), card, this.active_card);
                   this.active_card.active=false; //my
                   this.getCurrentHero().removeCard('hand', this.active_card);
                   this.active_card = null;
                   this.updateView();
               }
               else { alert('Now is not your turn');  } //my
            }
            }
    }

    onHeroSelect(targetHero, agent) {
        const heroId = parseInt(targetHero.getAttribute('id')[4]);
        let hero;

        heroId === this.current_turn ? hero = this.getCurrentHero() : hero = this.getOpponentHero();
        if(this.active_card) {
            this.active_card.action.perform(...this.getHeroes(), hero, this.active_card);
            this.active_card = null;
        }
        this.updateView();
    }

    getCurrentHero() {
        return this.heroes[this.current_turn];
    }

    getOpponentHero() {
        return this.heroes[3-this.current_turn];
    }

    getHeroes() {
        return [this.getCurrentHero(), this.getOpponentHero()]
    }

    cleanMinions() {
        Object.values(this.heroes).map(h => (h.minions = h.minions.filter(m => m.hp >0 )));
    }

    start(hero1, hero2) {
        this.hero1 = hero1;
        this.hero2 = hero2;
        this.heroes = {1: hero1, 2: hero2};
        Object.values(this.heroes).map(hero=> {
            [1, 1, 1].map(_=> hero.hand.push(hero.deck.splice(Math.floor(Math.random() * hero.deck.length), 1)[0]));
        });

        this.updateView();
    }

    updateView() {
        this.cleanMinions();

        this.view.deck1.innerHTML = this.hero1.deck.length;
        this.view.deck2.innerHTML = this.hero2.deck.length;
        this.view[this.current_turn === 2 ? 'playertable1' : 'playertable2'].classList.remove('active');
        this.view[this.current_turn === 1 ? 'playertable1' : 'playertable2'].classList.add('active');
        this.view.hero1_ability.innerHTML = `${this.hero1.current_manna} / ${this.hero1.manna}`;
        this.view.hero2_ability.innerHTML = `${this.hero2.current_manna} / ${this.hero2.manna}`;
        this.view.hand1.innerHTML = this.hero1.hand.map(c=>this.drawOneCard(c)).join('');
        this.view.hand2.innerHTML = this.hero2.hand.map(c=>this.drawOneCard(c)).join('');
        this.view.minions1.innerHTML = this.hero1.minions.map(c=>this.drawOneCard(c)).join('');
        this.view.minions2.innerHTML = this.hero2.minions.map(c=>this.drawOneCard(c)).join('');
        this.view.hero1hp.innerHTML = this.hero1.hp;
        this.view.hero2hp.innerHTML = this.hero2.hp;
        this.gameStatusCheck();
    }

    drawOneCard(card) {
        const cls = card === this.active_card ? 'active' : '';

        if(card.type === "minion") {
            return `
                <div class="card ${cls}" card-id="${card.id}">
                    <img src="images/${card.type}/${card.name}.png"  card-id="${card.id}">
                    <div class="stats">
                        <div class="attack">${card.attack}</div>
                        <div class="hp">${card.hp}</div>
                    </div>
                </div>`;
        } else {
            return `<div class="card ${cls}" card-id="${card.id}">
            <img src="images/${card.type}/${card.name}.png" card-id="${card.id}">
            </div>`
        }
    }


}
